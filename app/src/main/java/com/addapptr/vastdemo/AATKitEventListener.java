package com.addapptr.vastdemo;

import com.intentsoftware.addapptr.ad.VASTAdData;

public interface AATKitEventListener {

    void noAd(int placementId);

    void hasVASTAd(int placementId, VASTAdData data);
}
