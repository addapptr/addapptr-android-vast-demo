package com.addapptr.vastdemo.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.addapptr.vastdemo.R;
import com.addapptr.vastdemo.VastDemoApplication;
import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.VASTRequestParameters;

public class InStreamActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_stream);

        Spinner vastTypeSpinner = (Spinner) findViewById(R.id.vast_type_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.vast_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vastTypeSpinner.setAdapter(adapter);
        vastTypeSpinner.setOnItemSelectedListener(onItemSelectedListener());

        Button outStream = (Button) findViewById(R.id.button_out_stream);
        outStream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InStreamActivity.this, OutStreamActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AATKit.onActivityResume(this);
    }

    @Override
    protected void onPause() {
        AATKit.onActivityPause(this);
        super.onPause();
    }

    private AdapterView.OnItemSelectedListener onItemSelectedListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                VASTRequestParameters vastRequestParameters = new VASTRequestParameters();

                switch (position) {
                    case 0:
                        vastRequestParameters.videoType = VASTRequestParameters.VideoType.PreRoll;
                        break;
                    case 1:
                        vastRequestParameters.videoType = VASTRequestParameters.VideoType.MidRoll;
                        break;
                    case 2:
                        vastRequestParameters.videoType = VASTRequestParameters.VideoType.PostRoll;
                        break;
                    default:
                        break;
                }
                AATKit.setVASTRequestParameters(((VastDemoApplication) getApplication()).getVastPlacementId(), vastRequestParameters);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
    }
}
