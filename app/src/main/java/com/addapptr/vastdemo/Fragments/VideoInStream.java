package com.addapptr.vastdemo.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import com.addapptr.vastdemo.AATKitEventListener;
import com.addapptr.vastdemo.R;
import com.addapptr.vastdemo.SampleVideoPlayer;
import com.addapptr.vastdemo.VastDemoApplication;
import com.google.ads.interactivemedia.v3.api.AdDisplayContainer;
import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.AdsLoader;
import com.google.ads.interactivemedia.v3.api.AdsManager;
import com.google.ads.interactivemedia.v3.api.AdsManagerLoadedEvent;
import com.google.ads.interactivemedia.v3.api.AdsRequest;
import com.google.ads.interactivemedia.v3.api.ImaSdkFactory;
import com.google.ads.interactivemedia.v3.api.player.ContentProgressProvider;
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate;
import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.ad.VASTAdData;

public class VideoInStream extends Fragment implements AdEvent.AdEventListener, AdErrorEvent.AdErrorListener {

    private static String LOGTAG = "ImaExample";

    // The container for the ad's UI.
    private ViewGroup mAdUiContainer;

    // Factory class for creating SDK objects.
    private ImaSdkFactory mSdkFactory;

    // The AdsLoader instance exposes the requestAds method.
    private AdsLoader mAdsLoader;

    // AdsManager exposes methods to control ad playback and listen to ad events.
    private AdsManager mAdsManager;

    // Whether an ad is displayed.
    private boolean mIsAdDisplayed;

    private VASTAdData vastAdData;

    private SampleVideoPlayer mVideoPlayer;
    private SeekBar seekBarVideo;
    private Handler handler = new Handler();

    private Button buttonRestart;

    private Button buttonPlay;
    private Button buttonPause;

    private ProgressBar progressBar;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        seekBarVideo = (SeekBar) getActivity().findViewById(R.id.progress_bar_video);
        seekBarVideo.setVisibility(View.VISIBLE);
        seekBarVideo.getProgressDrawable().setColorFilter(Color.CYAN, PorterDuff.Mode.SRC_IN);
        seekBarVideo.getThumb().setColorFilter(Color.CYAN, PorterDuff.Mode.SRC_IN);

        buttonRestart = (Button) getActivity().findViewById(R.id.button_restart);
        buttonRestart.setOnClickListener(createOnRestartButtonClickListener());

        buttonPlay = (Button) getActivity().findViewById(R.id.button_play);
        buttonPause = (Button) getActivity().findViewById(R.id.button_pause);
        buttonPause.setVisibility(View.INVISIBLE);

        // Create an AdsLoader.
        mSdkFactory = ImaSdkFactory.getInstance();
        mAdsLoader = mSdkFactory.createAdsLoader(this.getContext());
        // Add listeners for when ads are loaded and for errors.
        mAdsLoader.addAdErrorListener(this);
        mAdsLoader.addAdsLoadedListener(adsLoadedListener());

        // Add listener for when the content video finishes.
        mVideoPlayer.addVideoCompletedListener(onVideoCompletedListener());

        // When Play is clicked, load VAST response and hide the button.
        buttonPlay.setOnClickListener(createOnPlayButtonClickListener());

        buttonPause.setOnClickListener(createOnButtonPauseClickListener());

        buttonPlay.setVisibility(View.VISIBLE);

        seekBarVideo.setMax(100);
        seekBarVideo.setProgress(mVideoPlayer.getCurrentPosition() * 100 / mVideoPlayer.getDuration());

        seekBarVideo.setOnSeekBarChangeListener(onSeekBarChangeListener());
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar);

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (mVideoPlayer.getDuration() != 0) {
                int startTime = mVideoPlayer.getCurrentPosition() * 100 / mVideoPlayer.getDuration();
                seekBarVideo.setProgress(startTime);
            }
            handler.postDelayed(this, 150);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_in_stream, container, false);

        mVideoPlayer = (SampleVideoPlayer) rootView.findViewById(R.id.sampleVideoPlayer);
        mAdUiContainer = (ViewGroup) rootView.findViewById(R.id.videoPlayerWithAdPlayback);

        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onAdEvent(AdEvent adEvent) {
        Log.i(LOGTAG, "Event: " + adEvent.getType());

        // These are the suggested event types to handle. For full list of all ad event
        // types, see the documentation for AdEvent.AdEventType.
        switch (adEvent.getType()) {
            case STARTED:
                AATKit.reportVASTImpression(vastAdData);
                break;
            case LOADED:
                // AdEventType.LOADED will be fired when ads are ready to be played.
                // AdsManager.start() begins ad playback. This method is ignored for VMAP or
                // ad rules playlists, as the SDK will automatically start executing the
                // playlist.
                mAdsManager.start();
                Log.d("duration ", String.valueOf(adEvent.getAd().getDuration()));
                seekBarVideo.getThumb().setColorFilter(Color.CYAN, PorterDuff.Mode.SRC_IN);
                seekBarVideo.setEnabled(false);
                break;
            case CONTENT_PAUSE_REQUESTED:
                // AdEventType.CONTENT_PAUSE_REQUESTED is fired immediately before a video
                // ad is played.
                mIsAdDisplayed = true;
                mVideoPlayer.pause();
                break;
            case CONTENT_RESUME_REQUESTED:
                // AdEventType.CONTENT_RESUME_REQUESTED is fired when the ad is completed
                // and you should start playing your content.
                mIsAdDisplayed = false;
                mVideoPlayer.play();
                break;
            case ALL_ADS_COMPLETED:
                if (mAdsManager != null) {
                    mAdsManager.destroy();
                    mAdsManager = null;
                }
                break;
            case TAPPED:
                AATKit.reportVASTClick(vastAdData);
                break;
            case COMPLETED:
                seekBarVideo.getThumb().setColorFilter(Color.CYAN, PorterDuff.Mode.SRC_IN);
                seekBarVideo.setEnabled(true);
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {
        Log.e(LOGTAG, "Ad Error: " + adErrorEvent.getError().getMessage());
        mVideoPlayer.play();
    }

    @Override
    public void onResume() {
        if (mAdsManager != null && mIsAdDisplayed) {
            mAdsManager.resume();
        } else {
            mVideoPlayer.play();
        }
        super.onResume();

        VastDemoApplication vastDemoApplication = (VastDemoApplication) getActivity().getApplication();
        vastDemoApplication.setAdListener(createAATKitEventListener());

    }

    @Override
    public void onPause() {
        if (mAdsManager != null && mIsAdDisplayed) {
            mAdsManager.pause();
        } else {
            mVideoPlayer.pause();
        }
        super.onPause();
    }

    public SeekBar.OnSeekBarChangeListener onSeekBarChangeListener() {

        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setProgress(seekBar.getProgress());
                mVideoPlayer.seekTo(seekBar.getProgress() * mVideoPlayer.getDuration() / 100);
            }
        };
    }

    public View.OnClickListener createOnButtonPauseClickListener() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoPlayer.pause();
                buttonPlay.setVisibility(View.VISIBLE);
                buttonPause.setVisibility(View.INVISIBLE);
            }
        };
    }

    public View.OnClickListener createOnRestartButtonClickListener() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoPlayer.pause();
                buttonPlay.setVisibility(View.VISIBLE);
                buttonPause.setVisibility(View.INVISIBLE);

                Intent intent = getContext().getPackageManager().getLaunchIntentForPackage(getContext().getPackageName());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        };
    }

    private AdsLoader.AdsLoadedListener adsLoadedListener() {
        return new AdsLoader.AdsLoadedListener() {
            @Override
            public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {
                // Ads were successfully loaded, so get the AdsManager instance. AdsManager has
                // events for ad playback and errors.
                mAdsManager = adsManagerLoadedEvent.getAdsManager();

                // Attach event and error event listeners.
                mAdsManager.addAdErrorListener(VideoInStream.this);
                mAdsManager.addAdEventListener(VideoInStream.this);
                mAdsManager.init();
            }
        };
    }

    private SampleVideoPlayer.OnVideoCompletedListener onVideoCompletedListener() {
        return new SampleVideoPlayer.OnVideoCompletedListener() {
            @Override
            public void onVideoCompleted() {
                // Handle completed event for playing post-rolls.
                if (mAdsLoader != null) {
                    mAdsLoader.contentComplete();
                }

            }
        };
    }

    private View.OnClickListener createOnPlayButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                int VASTPlacementId = ((VastDemoApplication) getActivity().getApplication()).getVastPlacementId();
                AATKit.reportAdSpaceForPlacement(VASTPlacementId);
                AATKit.reloadPlacement(VASTPlacementId);
            }
        };
    }

    private AATKitEventListener createAATKitEventListener() {
        return new AATKitEventListener() {
            @Override
            public void noAd(int placementId) {

                if (mVideoPlayer.getCurrentPosition() > 0) {
                    mVideoPlayer.start();
                } else {
                    buttonRestart.setEnabled(true);

                    mVideoPlayer.setVideoPath(getString(R.string.content_url));

                    handler.post(runnable);
                }

                buttonPause.setVisibility(View.VISIBLE);
                buttonPlay.setVisibility(View.INVISIBLE);

                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void hasVASTAd(int placementId, VASTAdData data) {
                vastAdData = data;

                if (mVideoPlayer.getCurrentPosition() > 0) {

                    mVideoPlayer.start();

                } else {

                    AdDisplayContainer adDisplayContainer = mSdkFactory.createAdDisplayContainer();
                    adDisplayContainer.setAdContainer(mAdUiContainer);

                    // Create the ads request.
                    AdsRequest request = mSdkFactory.createAdsRequest();
                    request.setAdsResponse(data.getXml()); //set the VAST response obtained using AATKit
                    request.setAdDisplayContainer(adDisplayContainer);
                    request.setContentProgressProvider(new ContentProgressProvider() {
                        @Override
                        public VideoProgressUpdate getContentProgress() {
                            if (mIsAdDisplayed || mVideoPlayer == null || mVideoPlayer.getDuration() <= 0) {
                                return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
                            }
                            return new VideoProgressUpdate(mVideoPlayer.getCurrentPosition(),
                                    mVideoPlayer.getDuration());
                        }
                    });

                    // Request the ad. After the ad is loaded, onAdsManagerLoaded() will be called.
                    mAdsLoader.requestAds(request);
                    buttonRestart.setEnabled(true);

                    mVideoPlayer.setVideoPath(getString(R.string.content_url));

                    handler.post(runnable);
                }

                buttonPause.setVisibility(View.VISIBLE);
                buttonPlay.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }
        };
    }
}