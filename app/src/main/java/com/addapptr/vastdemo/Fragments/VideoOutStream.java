package com.addapptr.vastdemo.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.addapptr.vastdemo.AATKitEventListener;
import com.addapptr.vastdemo.R;
import com.addapptr.vastdemo.SampleVideoPlayer;
import com.addapptr.vastdemo.VastDemoApplication;
import com.google.ads.interactivemedia.v3.api.AdDisplayContainer;
import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.AdsLoader;
import com.google.ads.interactivemedia.v3.api.AdsManager;
import com.google.ads.interactivemedia.v3.api.AdsManagerLoadedEvent;
import com.google.ads.interactivemedia.v3.api.AdsRequest;
import com.google.ads.interactivemedia.v3.api.ImaSdkFactory;
import com.google.ads.interactivemedia.v3.api.player.ContentProgressProvider;
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate;
import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.ad.VASTAdData;


public class VideoOutStream extends Fragment implements AdEvent.AdEventListener, AdErrorEvent.AdErrorListener {

    private static String LOGTAG = "ImaExample";

    // The container for the ad's UI.
    private ViewGroup mAdUiContainer;

    // Factory class for creating SDK objects.
    private ImaSdkFactory mSdkFactory;

    // The AdsLoader instance exposes the requestAds method.
    private AdsLoader mAdsLoader;

    // AdsManager exposes methods to control ad playback and listen to ad events.
    private AdsManager mAdsManager;

    // Whether an ad is displayed.
    private boolean mIsAdDisplayed;

    private VASTAdData vastAdData;

    private SampleVideoPlayer mVideoPlayer;

    private ProgressBar progressBar;

    private Button reload;
    private Button show;
    private TextView textView;

    private RelativeLayout frameVideoPlayer;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSdkFactory = ImaSdkFactory.getInstance();
        mAdsLoader = mSdkFactory.createAdsLoader(this.getContext());
        mAdsLoader.addAdErrorListener(this);
        mAdsLoader.addAdsLoadedListener(adsLoadedListener());

        mVideoPlayer.addVideoCompletedListener(onVideoCompletedListener());

        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar);

        reload = (Button) getActivity().findViewById(R.id.reload_button);
        show = (Button) getActivity().findViewById(R.id.show_button);
        textView = (TextView) getActivity().findViewById(R.id.havead_noad);

        reload.setOnClickListener(createOnReloadButtonClickListener());
        show.setOnClickListener(createOnShowButtonClickListener());

        frameVideoPlayer = (RelativeLayout) getActivity().findViewById(R.id.frame_video_player);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_out_stream, container, false);

        mVideoPlayer = (SampleVideoPlayer) rootView.findViewById(R.id.sampleVideoPlayer2);
        mAdUiContainer = (ViewGroup) rootView.findViewById(R.id.videoPlayerWithAdPlayback2);

        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onAdEvent(AdEvent adEvent) {
        Log.i(LOGTAG, "Event: " + adEvent.getType());

        // These are the suggested event types to handle. For full list of all ad event
        // types, see the documentation for AdEvent.AdEventType.
        switch (adEvent.getType()) {
            case STARTED:
                AATKit.reportVASTImpression(vastAdData);
                break;
            case LOADED:
                // AdEventType.LOADED will be fired when ads are ready to be played.
                // AdsManager.start() begins ad playback. This method is ignored for VMAP or
                // ad rules playlists, as the SDK will automatically start executing the
                // playlist.
                mAdsManager.start();
                Log.d("duration ", String.valueOf(adEvent.getAd().getDuration()));
                break;
            case CONTENT_PAUSE_REQUESTED:
                // AdEventType.CONTENT_PAUSE_REQUESTED is fired immediately before a video
                // ad is played.
                mIsAdDisplayed = true;
                mVideoPlayer.pause();
                break;
            case CONTENT_RESUME_REQUESTED:
                // AdEventType.CONTENT_RESUME_REQUESTED is fired when the ad is completed
                // and you should start playing your content.
                mIsAdDisplayed = false;
                mVideoPlayer.play();
                break;
            case ALL_ADS_COMPLETED:
                if (mAdsManager != null) {
                    mAdsManager.destroy();
                    mAdsManager = null;
                }
                frameVideoPlayer.setVisibility(View.INVISIBLE);
                break;
            case TAPPED:
                AATKit.reportVASTClick(vastAdData);
                break;
            case COMPLETED:
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {
        Log.e(LOGTAG, "Ad Error: " + adErrorEvent.getError().getMessage());
        mVideoPlayer.play();
    }

    @Override
    public void onResume() {
        if (mAdsManager != null && mIsAdDisplayed) {
            mAdsManager.resume();
        } else {
            mVideoPlayer.play();
        }
        super.onResume();

        VastDemoApplication vastDemoApplication = (VastDemoApplication) getActivity().getApplication();
        vastDemoApplication.setAdListener(createAATKitEventListener());
    }

    @Override
    public void onPause() {
        if (mAdsManager != null && mIsAdDisplayed) {
            mAdsManager.pause();
        } else {
            mVideoPlayer.pause();
        }
        super.onPause();
    }

    private AdsLoader.AdsLoadedListener adsLoadedListener() {
        return new AdsLoader.AdsLoadedListener() {
            @Override
            public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {
                // Ads were successfully loaded, so get the AdsManager instance. AdsManager has
                // events for ad playback and errors.
                mAdsManager = adsManagerLoadedEvent.getAdsManager();

                // Attach event and error event listeners.
                mAdsManager.addAdErrorListener(VideoOutStream.this);
                mAdsManager.addAdEventListener(VideoOutStream.this);
                mAdsManager.init();
            }
        };
    }

    private SampleVideoPlayer.OnVideoCompletedListener onVideoCompletedListener() {
        return new SampleVideoPlayer.OnVideoCompletedListener() {
            @Override
            public void onVideoCompleted() {
                // Handle completed event for playing post-rolls.
                if (mAdsLoader != null) {
                    mAdsLoader.contentComplete();
                }

            }
        };
    }

    private View.OnClickListener createOnReloadButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                textView.setText("");
                frameVideoPlayer.setVisibility(View.INVISIBLE);
                int VASTPlacementId = ((VastDemoApplication) getActivity().getApplication()).getVastPlacementId();
                AATKit.reportAdSpaceForPlacement(VASTPlacementId);
                AATKit.reloadPlacement(VASTPlacementId);
            }
        };
    }

    private View.OnClickListener createOnShowButtonClickListener() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView.setText("");
                frameVideoPlayer.setVisibility(View.VISIBLE);

                if (mVideoPlayer.getCurrentPosition() > 0) {

                    mVideoPlayer.start();

                } else {

                    AdDisplayContainer adDisplayContainer = mSdkFactory.createAdDisplayContainer();
                    adDisplayContainer.setAdContainer(mAdUiContainer);

                    // Create the ads request.
                    AdsRequest request = mSdkFactory.createAdsRequest();
                    request.setAdsResponse(vastAdData.getXml()); //set the VAST response obtained using AATKit
                    request.setAdDisplayContainer(adDisplayContainer);
                    request.setContentProgressProvider(new ContentProgressProvider() {
                        @Override
                        public VideoProgressUpdate getContentProgress() {
                            if (mIsAdDisplayed || mVideoPlayer == null || mVideoPlayer.getDuration() <= 0) {
                                return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
                            }
                            return new VideoProgressUpdate(mVideoPlayer.getCurrentPosition(),
                                    mVideoPlayer.getDuration());
                        }
                    });

                    // Request the ad. After the ad is loaded, onAdsManagerLoaded() will be called.
                    mAdsLoader.requestAds(request);
                }

            }
        };
    }

    private AATKitEventListener createAATKitEventListener() {
        return new AATKitEventListener() {
            @Override
            public void noAd(int placementId) {

                progressBar.setVisibility(View.INVISIBLE);
                textView.setText("No ad");
            }

            @Override
            public void hasVASTAd(int placementId, VASTAdData data) {
                vastAdData = data;

                progressBar.setVisibility(View.INVISIBLE);
                textView.setText("Ad loaded");
            }
        };
    }
}