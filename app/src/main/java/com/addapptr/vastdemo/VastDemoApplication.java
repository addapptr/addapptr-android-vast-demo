package com.addapptr.vastdemo;

import android.app.Application;
import android.content.Context;

import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.BannerPlacementLayout;
import com.intentsoftware.addapptr.PlacementSize;
import com.intentsoftware.addapptr.ad.VASTAdData;

public class VastDemoApplication extends Application implements AATKit.Delegate {

    private int vastPlacementId = -1;
    private AATKitEventListener listener;

    @Override
    public void onCreate() {
        super.onCreate();
        AATKit.init(this, this);

        vastPlacementId = AATKit.createPlacement("VAST", PlacementSize.VAST);
    }

    public int getVastPlacementId() {
        return vastPlacementId;
    }

    @Override
    public void aatkitHaveAd(int placementId) {
    }

    @Override
    public void aatkitNoAd(int placementId) {
        if (listener != null)
            listener.noAd(placementId);
    }

    @Override
    public void aatkitPauseForAd(int placementId) {
    }

    @Override
    public void aatkitResumeAfterAd(int placementId) {
    }

    @Override
    public void aatkitShowingEmpty(int placementId) {
    }

    @Override
    public void aatkitUserEarnedIncentive(int placementId) {
    }

    @Override
    public void aatkitObtainedAdRules(boolean fromTheServer) {
    }

    @Override
    public void aatkitUnknownBundleId() {
    }

    @Override
    public void aatkitHaveAdForPlacementWithBannerView(int placementId, BannerPlacementLayout bannerView) {
    }

    @Override
    public void aatkitHaveVASTAd(int placementId, VASTAdData data) {
        if (listener != null) {
            listener.hasVASTAd(placementId, data);
        }
    }

    public void setAdListener(AATKitEventListener listener) {
        this.listener = listener;
    }
}
